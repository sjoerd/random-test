#!/bin/sh
set -ve

K=https://repositories.apertis.org/apertis/pool/target/k/krb5/libk5crypto3_1.17-1ba1_amd64.deb
R=https://repositories.apertis.org/apertis/pool/development/r/rsync/rsync_3.1.3-1co1ba1_amd64.deb

git clean -q -dfx
wget -q ${K}
wget -q ${R}
echo "Download done $(date)"
if ! sha256sum -c checksums ; then
  echo "FAILED expected:"
  cat checksums
  echo "GOT:"
  sha256sum *
  exit 1
fi
